﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WindowsServiceTest
{
    partial class Service2 : ServiceBase
    {
        public Service2()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            System.IO.File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log.txt"),
                $"{DateTime.Now} {nameof(Service2)} 开始...\r\n");
        }

        protected override void OnStop()
        {
            System.IO.File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log.txt"),
                $"{DateTime.Now} {nameof(Service2)} 停止...\r\n");
        }
    }
}
